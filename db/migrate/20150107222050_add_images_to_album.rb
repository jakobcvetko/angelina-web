class AddImagesToAlbum < ActiveRecord::Migration
  def change
    add_column :albums, :images, :text
  end
end
