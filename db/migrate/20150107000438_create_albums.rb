class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :image
      t.string :hover_image
      t.text :name

      t.timestamps
    end
  end
end
