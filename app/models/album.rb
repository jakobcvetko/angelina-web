class Album < ActiveRecord::Base

  ### Attributes
  #
  # image
  # hover_image
  # images
  # name
  # position
  #

  serialize :name, Hash
  serialize :images, Array
  # mount_uploader :image, AlbumImageUploader
  # mount_uploader :hover_image, AlbumImageUploader
  # mount_uploaders :images, ImageUploader

  def localized_name locale=I18n.locale
    name[locale]
  end

  I18n.available_locales.each do |locale|
    define_method "#{locale}_name" do
      name[locale]
    end

    define_method "#{locale}_name=" do |val|
      name[locale] = val
    end
  end

  def image_presigned_post
    @image_presigned_post ||= S3_BUCKET.presigned_post(key: "uploads/#{SecureRandom.uuid}/${filename}", success_action_status: 201, acl: :public_read)
  end

end
