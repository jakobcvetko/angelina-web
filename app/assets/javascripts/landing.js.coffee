#= require jquery
#= require jquery_ujs
#= require turbolinks
#= require bootstrap

$ ->
  $(".carousel").carousel
    interval: 6000,
    pause: false

