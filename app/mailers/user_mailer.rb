class UserMailer < ActionMailer::Base
  default from: "info@n-angelina.si"

  def web_message(post, to=nil)
    @post = post
    mail(to: (to || admin_email), subject: 'Novo sporočilo iz strani www.n-angelina.si')
  end

  def admin_email
    ENV['ADMIN_EMAIL'] || "info@n-angelina.si"
  end
end
