module ApplicationHelper

  def active_for page
    'active' if page.to_s == params[:action]
  end

end
