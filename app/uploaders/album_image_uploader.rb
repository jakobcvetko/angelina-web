# encoding: utf-8

class AlbumImageUploader < CarrierWave::Uploader::Base

  def store_dir
    "uploads/album#{model.id}/#{mounted_as}"
  end
end
