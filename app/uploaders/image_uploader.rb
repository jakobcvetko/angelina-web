# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  def store_dir
    "uploads/album#{model.id}/images"
  end

  process :resize_to_fit => [1000, 900]

  version :thumb do
    process :resize_to_fill => [150, 150]
  end
end
