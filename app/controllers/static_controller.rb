class StaticController < ApplicationController
  layout 'application'

  def landing
    render layout: false
  end

  def home
    render render_name
  end

  def pricing
  end

  def about
    render render_name
  end

  def map
    render render_name
  end

  def events
    # render render_name
  end

  def culture
    # render render_name
  end

  def sport
    # render render_name
  end

  def contact
    render render_name
  end

  def writeus
    # render render_name
  end

  def handle_message
    if valid_params?(params[:post])
      Rollbar.info("Message sent from web", post: params[:post])

      UserMailer.web_message(params[:post]).deliver
      UserMailer.web_message(params[:post], params[:post][:email]).deliver if params[:post][:copy]

      flash[:notice] = "Success"
      redirect_to :static_writeus
    else
      flash[:notice] = "Invalid input data"
      redirect_to :static_writeus
    end

  end

  def gallery
  end

  def gallery_content
    @album = Album.find(params[:id])
  end

  private
  def render_name
    [params[:controller], 2.times.map{ params[:action] }].flatten.join('/')
  end

  def valid_params? attrs
    return false unless attrs
    return false unless attrs[:email]
    return false unless attrs[:message]
    true
  end
end
