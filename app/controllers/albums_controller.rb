class AlbumsController < ApplicationController
  before_filter :authenticate
  before_action :set_album, only: [:show, :edit, :update, :destroy, :remove_image, :remove_images]

  layout 'management'

  # GET /albums
  # GET /albums.json
  def index
    @albums = Album.all
  end

  # GET /albums/1
  # GET /albums/1.json
  def show
  end

  # GET /albums/new
  def new
    @album = Album.new
  end

  # GET /albums/1/edit
  def edit
  end

  # POST /albums
  # POST /albums.json
  def create
    @album = Album.new(album_params)

    respond_to do |format|
      if @album.save
        format.html { redirect_to albums_path }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /albums/1
  # PATCH/PUT /albums/1.json
  def update
    attrs = album_params

    if attrs[:images]
      @album.images += attrs.delete(:images)
    end

    respond_to do |format|
      if @album.update(attrs)
        # format.html { redirect_to edit_album_path(@album) }
        format.html { redirect_to albums_path }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /albums/1
  # DELETE /albums/1.json
  def destroy
    @album.destroy
    respond_to do |format|
      format.html { redirect_to albums_url }
      format.json { head :no_content }
    end
  end

  def remove_image
    if @album.images.count == 1
      @album.images = []
    else
      @album.images.delete_at(params[:id].to_i)
      @album.images = @album.images
    end

    @album.save
    # redirect_to edit_album_path(@album)
  end

  def remove_images
    @album.images = []
    @album.save
    redirect_to edit_album_path(@album)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_album
      @album = Album.find(params[:album_id] || params[:id])
    end

    def locale_names
      I18n.available_locales.map do |locale|
        [locale, :name].join('_').to_sym
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def album_params
      permitted_params = [:position, :image, :image_cache, :hover_image, :hover_image_cache, images: []] + locale_names
      params.require(:album).permit(permitted_params)
    end
end
