# Assets precompilcation

Rails.application.config.assets.precompile += %w( landing.css )
Rails.application.config.assets.precompile += %w( landing.js )

Rails.application.config.assets.precompile += %w( angelina.css )
Rails.application.config.assets.precompile += %w( angelina.js )

Rails.application.config.assets.precompile += %w( fancydropdown.css fancydropdown_ie.css fancydropdown.js )
