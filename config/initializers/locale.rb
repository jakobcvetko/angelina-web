# Locales setup

# I18n.load_path += Dir[Rails.root.join('lib', 'locale', '*.{rb,yml}')]
# I18n.config.enforce_available_locales = false

I18n.enforce_available_locales = true
I18n.available_locales = [:sl, :en, :de, :it, :ru]
I18n.default_locale = :sl


