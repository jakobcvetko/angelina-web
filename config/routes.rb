Rails.application.routes.draw do

  root controller: :static, action: :landing

  namespace :static, path: '/' do
    get :home
    get :pricing
    get :about
    get :map
    get :events
    get :culture
    get :sport
    get :contact
    get :writeus

    post :writeus, action: :handle_message

    get :gallery
    get 'gallery_content/:id', action: :gallery_content, as: :gallery_content
  end

  resources :albums do
    delete 'images/:id', action: :remove_image, as: :remove_image
    delete 'images', action: :remove_images, as: :remove_images
  end
end
